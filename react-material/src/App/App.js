import './App.css';
import SideMenu from '../components/SideMenu';
import { createMuiTheme, CssBaseline, makeStyles } from '@material-ui/core';
import { ThemeProvider } from '@material-ui/core/styles';
import Header from '../components/Header';
import Employee from '../pages/Employees/Employee';


const theme = createMuiTheme({
  palette:{
    primary:{
      main: '#333996',
      light:'#3c44b126'
    },
    secondary:{
      main: '#f8324526',
      light:'#f8324526'
    },
    background:{
      default: '#f4f5fd'
    },
  },
  shape:{
    borderRadius: '12px'
  },
  overrides:{
    MuiAppBar:{
      root:{
        transform: 'translateZ(0)'
      }
    }
  },
  props:{
    MuiIconButton:{
      disableRipple:true
    }
  }
})

const useStyles = makeStyles({
    appMain:{
      paddingLeft:'320px',
      width:'100%'
    }
})

function App() {
  const classes = useStyles();
  return (
    //short representation react fragment
    <ThemeProvider theme = {theme}>
    <SideMenu />
    <div className = {classes.appMain}>
      <Header/>
     
      <Employee/>
    </div>
    {/* cssbaseline helps to keep all required common css to our project */}
    <CssBaseline />
    </ThemeProvider>
  );
}

export default App;
