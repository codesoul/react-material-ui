import React  from 'react'
import { FormControlLabel , FormControl, Checkbox } from '@material-ui/core'



export default function CheckBox(props) {
    
    const {name, label, value, onChange} = props

    const convertToDefEventPara = (name, value) =>({
        target:{
            name, value
        }
    })

    return (
        <FormControl>
            <FormControlLabel control = {<Checkbox
                name = {name}
                color="primary"
                checked = {value}
                onChange = {e => onChange(convertToDefEventPara(name, e.target.checked))}
            />}
            label = {label}
            />
        </FormControl>
    )
}
