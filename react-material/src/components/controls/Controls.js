import Input from './Input';
import RadioGroups from './RadioGroups';
import Select from './Select';
import CheckBox from './CheckBox';
import DatePicker from './DatePicker';
import Button from './Button';

const Controls = {
    Input, RadioGroups, Select , CheckBox, DatePicker, Button
}

export default Controls;