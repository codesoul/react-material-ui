import React from 'react'
import { AppBar, Toolbar, Grid, InputBase, IconButton, Badge } from '@material-ui/core';
import { makeStyles } from '@material-ui/core';
import NotificationsNoneIcon from '@material-ui/icons/NotificationsNone';
import ChatBubbleOutlineIcon from '@material-ui/icons/ChatBubbleOutline';
import PowerSettingsNewIcon from '@material-ui/icons/PowerSettingsNew';
import SearchIcon from '@material-ui/icons/Search';


const useStyles = makeStyles({
    root:{
        backgroundColor:'#fff',
    },
    searchInput:{
        opacity:'0.6',
        padding: '0px 8px',
        fontSize: '0.8rem',
        borderRadius:'8px',
        '&:hover':{
            backgroundColor:'#f2f2f2',
        },
        '& .MuiSvgIcon-root':{
            marginRight:'8px'
        }
    },
    btnRoot:{
        backgroundColor:'green'
    },
    btnLabel:{
        backgroundColor:'red'
    }

})


export default function Header() {

    const classes = useStyles();

    return (
        <AppBar position="static" className = {classes.root}>
            <Toolbar>
                <Grid container
                alignItems="center"
                >
                    {/* </Grid><Grid item sm={4} style={{ border: '1px solid #fff' }}> */}
                    <Grid item >
                        <InputBase 
                        placeholder = "Search topics"
                        className = {classes.searchInput}
                        startAdornment = {<SearchIcon fontSize = "small"/>}
                        />
                    </Grid>

                    <Grid item sm></Grid>

                    <Grid item  >
                        {/* using classes and focussing root and child */}
                        {/* <IconButton classes = {{root:classes.btnRoot, label:classes.btnLabel}}> */}
                        <IconButton >
                            <Badge badgeContent={4} color="secondary">
                                <NotificationsNoneIcon fontSize = "small" />
                            </Badge>
                        </IconButton>

                        <IconButton>
                            <Badge badgeContent={4} color="primary">
                                <ChatBubbleOutlineIcon  fontSize = "small"/>
                            </Badge>
                        </IconButton>

                        <IconButton>
                            <Badge badgeContent={4} color="primary">
                                <PowerSettingsNewIcon fontSize = "small"/>
                            </Badge>
                        </IconButton>

                    </Grid>
                </Grid>
            </Toolbar>
        </AppBar>
    )
}
