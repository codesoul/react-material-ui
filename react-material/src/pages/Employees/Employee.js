import React from 'react';
import PageHeader from '../../components/PageHeader';
import PeopleOutlineTwoToneIcon from '@material-ui/icons/PeopleOutlineTwoTone';
import EmployeeForm from './EmployeeForm';
import {makeStyles, Paper} from '@material-ui/core';

const useStyles = makeStyles(theme =>({
  pageContent:{
    margin: theme.spacing(5),
    padding: theme.spacing(3)
  }
}))


export default function Employee() {

    const classes = useStyles();
    return (
        <>
        <PageHeader 
        title = "Page Header"
        subTitle = "Page Description"
        icon = {<PeopleOutlineTwoToneIcon fontSize = "large" />}
      />  

      <Paper className = {classes.pageContent}>
      <EmployeeForm/>
      </Paper>
      </>
    )
}
