import React, {useState, useEffect} from 'react';
import { Grid} from '@material-ui/core';
import {useForm, Form} from '../../components/useForm';
import Controls from '../../components/controls/Controls';
import * as employeeService from '../../services/employeeService';


const genderItems = [
    {id:'male', title: 'Male'},
    {id:'female', title: 'Female'},
    {id:'others', title: 'Others'}
]

const initialValues = {
    id:0,
    fullName:'',
    email:'',
    mobile:'',
    city:'',
    gender:'male',
    departmentid:'',
    hireDate:new Date(),
    isPermanent:false,
}

export default function EmployeeForm() {
    
    
   

    const {values, setValues, handleInputChange} = useForm(initialValues);
    
   

    return (
            <Form>
            <Grid container>
                <Grid item xs = {6}>

                    <Controls.Input
                        name = "fullName"
                        label = "Full Name"
                        value = {values.fullName}
                        onChange = {handleInputChange}
                    />

                    <Controls.Input 
                       name = "mobile"
                       label = "Mobile"
                       value = {values.mobile}
                       onChange = {handleInputChange}  
                    />

                    <Controls.Input 
                       name = "city"
                       label = "City"
                       value = {values.city}
                       onChange = {handleInputChange}  
                    />

                    <Controls.Input 
                       name = "email"
                       label = "Email"
                       value = {values.email}
                       onChange = {handleInputChange}  
                    />

                </Grid>

                <Grid item xs = {6}>

                    <Controls.RadioGroups 
                     name="gender"
                     label = "Gender"
                     value={values.gender}
                     onChange={handleInputChange}
                     items = {genderItems}   
                    />

                    <Controls.Select
                    name = "departmentid"
                    label = "Department"
                    value = {values.departmentid}
                    onChange = {handleInputChange}
                    options = {employeeService.getDepartmentCollection()}
                    />

                    <Controls.DatePicker
                        name = "hireDate"
                        label = "Hire Date"
                        value = {values.hireDate}
                        onChange = {handleInputChange}
                    />
                    
                    <Controls.CheckBox 
                        name = "isPermanent"
                        label = "Permanent Employee"
                        value = {values.isPermanent}
                        onChange = {handleInputChange}
                    />

         <div>
              <Controls.Button
               variant = "contained"
               color="primary"
               size = "large"
               text = "Submit"/>  
        </div>          

                   
                </Grid>
            </Grid>
            </Form>
    )
}
